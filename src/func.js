const getSum = (str1, str2) => {
    if (
        isNaN(+str1 || +str2) ||
        Array.isArray(str1 || str2) ||
        Number.isInteger(str1 || str2)
      ) {
        return false;
      }
      return "" + (+str1 + +str2);
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
    let countComments = 0;
    let countPosts = 0;
  
    listOfPosts.forEach(post => {
      post.author === authorName ? countPosts++ : null;
  
      if(post.comments) {
        post.comments.forEach(comment => {
          comment.author === authorName ? countComments++ : null;
        })
      }
    })
    return `Post:${countPosts},comments:${countComments}`;
};

const tickets=(people)=> {
    const priceForOneTicket = 25;
  let cashRegister = 0;

  people = people.map((el) => +el);

  for (let i = 0; i < people.length; i++) {
    if (people[i] === priceForOneTicket) {
      cashRegister += people[i];
    } else if (people[i] > priceForOneTicket) {
      let difference = people[i] - priceForOneTicket;

      cashRegister -= difference;
      cashRegister += priceForOneTicket;

      if (cashRegister < 0) {
        return 'NO';
      }
    }
  }
  return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
